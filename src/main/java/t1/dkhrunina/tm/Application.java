package t1.dkhrunina.tm;

import t1.dkhrunina.tm.constant.ArgumentConst;
import t1.dkhrunina.tm.constant.CommandConst;
import t1.dkhrunina.tm.util.TerminalUtil;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** Welcome to Task Manager ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nEnter command: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.out.println("[ERROR]");
        System.err.println("Input arguments are not correct.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[ERROR]");
        System.err.println("Command is not correct.");
    }

    private static void processCommand(final String arg) {
        switch (arg) {
            case CommandConst.VERSION: {
                showVersion();
                break;
            }
            case CommandConst.ABOUT: {
                showAbout();
                break;
            }
            case CommandConst.HELP: {
                showHelp();
                break;
            }
            case CommandConst.EXIT: {
                exit();
                break;
            }
            default: {
                showCommandError();
                break;
            }
        }
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case ArgumentConst.VERSION: {
                showVersion();
                break;
            }
            case ArgumentConst.ABOUT: {
                showAbout();
                break;
            }
            case ArgumentConst.HELP: {
                showHelp();
                break;
            }
            default: {
                showArgumentError();
                break;
            }
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show available actions.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Exit the application.\n", CommandConst.EXIT);
    }
}